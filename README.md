# Read folder then gen md5



## 用途
用於監控資料夾是否被變動。

## 使用環境 
Linux

## 使用方法
- read_Folder_then_gen_md5.py /var/www/html
- read_Folder_then_gen_md5.py /var/www/html /var/log/apache2
- read_Folder_then_gen_md5.py (使用者輸入)

## 輸出結果
- {'/var/www/html': 'cb41b03849b6b2aa9bc8830be6d7c995', '/var/log/apache2': 'cf3200ab898611d23f459c0d1423ee80'}
- {'/var/www/html': 'cb41b03849b6b2aa9bc8830be6d7c995'}

## 操作思維
將整個目錄排除因權限不夠無法讀取的檔案全數讀取後產製MD5，接下來為了避免檔案數量過多造成網路負擔，再次將整個物件進行MD5處理進而呈現在主控台