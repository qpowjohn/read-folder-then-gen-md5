import os
import sys
import os.path
import hashlib
import json
import logging
import module.check_os_is_linux
import module.check_path_is_exists

LogPath = "Log"
if not os.path.isdir(LogPath):
    os.makedirs(LogPath)
logging.basicConfig(filename="Log/debug.log", level=logging.DEBUG,
                    filemode="a", format="%(asctime)s %(name)s %(levelname)s %(message)s")
log = logging.getLogger('main')

log.debug("開始判斷是否為Linux環境")
if not module.check_os_is_linux.CheckOSisLinux():
    log.warn("執行環境非Linux，程式結束")
    os._exit(0)

log.debug("開始判斷是否有參數，若無參數則強制使用者輸入")

PathList = []
if len(sys.argv[1:]) > 0:
    log.debug("判斷到程式執行時有參數，開始判斷路徑存在與否")
    for path in sys.argv[1:]:
        module.check_path_is_exists.check_path_is_exists(path, PathList)
else:
    log.debug("判斷到程式執行時無參數，開始強制使用者輸入路徑")
    n = 0
    errorLimit = 3
    while n <= errorLimit:
        print(n)
        path = input("請輸入欲檢查的路徑(多路徑以空格分開): ")
        log.debug("使用者自行輸入: " + path)
        module.check_path_is_exists.check_path_is_exists(path, PathList)
        if len(PathList) >= 1:
            n = errorLimit+1
        else:
            n += 1
            log.info("嘗試錯誤次數第"+str(n)+"次")
            if n == errorLimit:
                log.error("嘗試錯誤次數達三次，關閉程式")
                print("錯誤次數過多，關閉程式")
                os._exit(0)
result = {}
for path in PathList:
    log.info("開始分析資料，path: "+ str(path))
    data = {}
    traversalFullName = [os.path.join(dirpath, f) for (
        dirpath, dirnames, filenames) in os.walk(path) for f in filenames]
    for FullName in traversalFullName:
        try:
            with open(FullName, 'rb') as file_to_check:
                _data = file_to_check.read()
                md5_data = hashlib.md5(_data).hexdigest()
                data[FullName] = md5_data
        except PermissionError:
            pass
        except Exception as e:
            log.error(str(e))
    data = hashlib.md5(json.dumps(
        data, sort_keys=True).encode('utf-8')).hexdigest()
    log.info("本次比對路徑: "+str([path])+"，檔案數: "+str(len(traversalFullName))+"，hash: "+str(data))
    result[path] = data

print(result)
