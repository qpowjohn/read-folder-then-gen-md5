import os
import logging

logger = logging.getLogger(__name__)


def check_path_is_exists(path, PathList):
    if not isinstance(path, str):
        logger.error("參數path非str")
    if not isinstance(PathList, list):
        logger.error("參數PathList非list")
    for _path in path.split(" "):
        if os.path.exists(_path):
            logger.debug(_path+" 存在於此機器，添加進PathList")
            PathList += [_path]
            logger.debug(PathList)
        else:
            logger.info(_path+" 不存在於此機器")
    return PathList
